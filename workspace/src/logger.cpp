//#define N 50

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <string.h>

//#include "Puntuaciones.h"
//#include "glut.h"

//de los jugadores anota un punto.
//La comunicacion entre tenis y logger se realiza mediante una tuberia con
//nombre

int main (int argc, char* argv[])
{
int tuberia_fifo;
char buffer[100];

//////////////////////////////////////////////////////////

////////////////////////////////////////////////////

//creando la tuberia fifo
if(mkfifo("/tmp/myfifo", 0777)<0){
	perror("Error al crear fifo");
	return 1;
}

//////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////

//abriendo la tuberia fifo
tuberia_fifo=open("/tmp/myfifo", O_RDONLY);

if(tuberia_fifo<0){
	perror("Error al abrir la tuberia");
	return 1;
}

while(read(tuberia_fifo, buffer, 100)!=0){
	printf("%s\n", buffer);
}

//////////////////////////////////////////////////////////////////
if(close(tuberia_fifo)<0){
	perror("close");
	return 1;
}


if(unlink("/tmp/myfifo")<0){
	perror("error en unlink");
	return 1;
}

/////////////////////////////////////////////////////////////////////

return 0;
}

