// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#define N 100

#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
//#include "Puntuaciones.h"

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>

#include <pthread.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void* hilo_comandos(void* d){
	CMundo* p=(CMundo*)d;
	p->RecibeComandosJugador();
}

void CMundo::RecibeComandosJugador(){
	while(1){
		usleep(10);
		char cad[100];
<<<<<<< HEAD
//		s_comunicacion.Receive(cad, sizeof(cad));
		read(fifo_teclas, cad, sizeof(cad));
=======
		s_communication.Receive(cad, sizeof(cad));
>>>>>>> practica5_52055
		unsigned char key;
		sscanf(cad, "%c", &key);
		if(key=='s')jugador1.velocidad.y=-4;
		if(key=='w')jugador1.velocidad.y=4;
		if(key=='l')jugador2.velocidad.y=-4;
		if(key=='o')jugador2.velocidad.y=4;
	}
}

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	//GAMEOVER
	char gameover[100];
	sprintf(gameover, "GAMEOVER");
	write(myfifo,gameover,strlen(gameover)+1);
	//CERRANDO TUBERIA FIFO DE LOGGER
	if(close(myfifo)<0){
		perror("close");
		exit(1);
	}
<<<<<<< HEAD

	//CREANDO TUBERIA FIFO SERVIDOR-CLIENTE
	if(close(fifo_sc)<0){
		perror("close");
		exit(1);
	}

	//CREANDO TUBERIA FIFO DE CLIENTE-SERVIDOR
	if(close(fifo_teclas)<0){
		perror("close");
		exit(1);
	}
=======
>>>>>>> practica5_52055
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	char str1[N];
	char str2[N];
//	Puntuaciones puntos;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		sprintf(str2, "Jugador 2 ha marcado, lleva %d puntos", puntos2);
		if(write(myfifo, str2, strlen(str2)+1)<0){
			perror("error en write");
			exit(1);
		}
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		sprintf(str1, "Jugador 1 ha marcado, lleva %d puntos", puntos1); 
		if(write(myfifo, str1, strlen(str1)+1)<0){
			perror("error en write");
			exit(1);
		}

	}
<<<<<<< HEAD

        int estadoActual=0, estadoAnterior=0;
        char str[100];
        if(puntos1==3){
                estadoActual=1;
                if(estadoActual!=estadoAnterior){
                        sprintf(str, "Jugador1 ha ganado\n");
                        write(myfifo, str, strlen(str)+1);
                }
                estadoAnterior=estadoActual;
                exit(0);
        }
        if(puntos2==3){
                estadoActual=1;
                if(estadoActual!=estadoAnterior){
                        sprintf(str, "Jugador2 ha ganado\n");
                        write(myfifo, str, strlen(str)+1);
                }
                estadoAnterior=estadoActual;
                exit(0);
        }
	

	////COMUNICACION POR SOCKETS
	//ENVIO DE DATOS DE SERVIDOR A CLIENTE
	char cad[200];
	sprintf(cad, "%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x, esfera.centro.y, jugador1.x1, jugador1.y1, jugador1.x2, jugador1.y2,jugador2.x1, jugador2.y1, jugador2.x2, jugador2.y2, puntos1, puntos2);
	if(write(fifo_sc, cad, sizeof(cad))<0){
		perror("write");
		exit(1);
	}

=======
	int estadoActual=0, estadoAnterior=0;
    	char str[100];
    	if(puntos1==3){
          	estadoActual=1;
        	if(estadoActual!=estadoAnterior){
              	sprintf(str, "Jugador1 ha ganado\n");
           		write(myfifo, str, strlen(str)+1);
 		}
	estadoAnterior=estadoActual;
	exit(0);
       	}
      	if(puntos2==3){
            	estadoActual=1;
          	if(estadoActual!=estadoAnterior){
                	sprintf(str, "Jugador2 ha ganado\n");
			write(myfifo, str, strlen(str)+1);
		}
		estadoAnterior=estadoActual;
		exit(0);
	}
//envio de datos de servidor a cliente
	char cad[200];
	sprintf(cad, "%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x, esfera.centro.y, jugador1.x1, jugador1.y1, jugador1.x2, jugador1.y2, jugador2.x1, jugador2.y1, jugador2.x2, jugador2.y2, puntos1, puntos2);
	s_communication.Send(cad, sizeof(cad));
>>>>>>> practica5_52055
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;


	}
}

void CMundo::Init()
{

	char IP[]="127.0.0.1";
	if(s_connection.InitServer(IP, 8000)<0){
		perror("ERROR OPENING SERVER\n");
		exit(1);
	}

	s_communication=s_connection.Accept();

	char name[N];
	s_communication.Receive(name, sizeof(name));

	printf("%s has joined the session\n", name);
///////////////////////////////////////////////////////
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//ABRIENDO LA TUBERÍA DE MENSAJE PUNTOS
	if ((myfifo=open("/tmp/myfifo", O_WRONLY))<0){
		perror("open");
		exit(1);
	}

<<<<<<< HEAD
	//ABRIENDO LA TUBERIA FIFO DE SERVIDOR-CLIENTE
	if((fifo_sc=open("/tmp/fifo_sc", O_WRONLY))<0){
		perror("open");
		exit(1);
	}

	//CREANDO THREAD QUE QUEDE A LA ESPERA DE LOS MENSAJES DEL CLIENTE
	pthread_create(&thid1, NULL, hilo_comandos, this);

	//ABRIENDO LA TUBERIA FIFO DE CLIENTE-SERVIDOR
	if((fifo_teclas=open("/tmp/fifo_teclas", O_RDONLY))<0){
		perror("open");
		exit(1);
	}

=======
//CREANDO THREAD QUE QUEDE A LA ESPERA DE LOS MENSAJES DEL CLIENTE
pthread_create(&thid1, NULL, hilo_comandos, this);

>>>>>>> practica5_52055
}

