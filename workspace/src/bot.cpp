#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>

#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"

int main(int argc, char* argv[]){
	int fd;
	DatosMemCompartida *pshared_data;
	char* projection;
	fd=open("/tmp/bot_data.txt", O_RDWR);
	if(fd<0){
		perror("Error en la apertura del fichero");
		return 1;
	}

	projection=(char*)mmap(NULL,sizeof(*(pshared_data)), PROT_WRITE|PROT_READ, MAP_SHARED, fd, 0);

	close (fd);

	pshared_data=(DatosMemCompartida*)projection;

while(1){
	usleep(2500);
	float posRaqueta1;
	posRaqueta1=((pshared_data->raqueta1.y2+pshared_data->raqueta1.y1)/2);
	if(posRaqueta1<pshared_data->esfera.centro.y)
		pshared_data->accion=1;
	else if(posRaqueta1>pshared_data->esfera.centro.y)
		pshared_data->accion=-1;
	else
		pshared_data->accion=0;
}

//while(1){
//	float posRaqueta2Futuro=0, posRaqueta2;
//	posRaqueta2=((pshared_data->raqueta2.y2+pshared_data->raqueta2.y1)/2);
  //     if(pshared_data->accion==0){
//		sleep(10);
//		while(1){
//			usleep(25000);
//			if(posRaqueta2<pshared_data->esfera.centro.y){
  //            		pshared_data->accion=1;
//				sleep(5);}
//				//posRaqueta2Futuro=1;
  //    		else if(posRaqueta2>pshared_data->esfera.centro.y)
    //            		pshared_data->accion=-1;
//				
  //      		else
    //            		pshared_data->accion=0;
//		}
//	}
//}

munmap(projection, sizeof(*(pshared_data)));

}
 
